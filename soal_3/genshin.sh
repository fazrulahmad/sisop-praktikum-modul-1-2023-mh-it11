#!/bin/bash

wget --no-check-certificate "https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2" -O genshin.zip
unzip genshin.zip
unzip genshin_character.zip

csv_file="list_character.csv"
directory="praktikumsisop"

# mastiin file csv nya ada
if [ ! -f "$csv_file" ]; then
    echo "File CSV tidak ditemukan: $csv_file"
    exit 1
fi

# ngehapus semua karakter non-printable
rename_files() {
  for file in *.jpg; do
      # ngambil nama file
      filename=$(basename "$file")

      # hapus semua karakter non-printable
      cleaned_name=$(echo "$filename" | tr -cd '[:print:]')

      # decode nama file dari Base64
      decoded_name=$(echo "$cleaned_name" | base64 -d)

      # cari nama yang cocok dari file csv
      matched_line=$(grep "$decoded_name" "$csv_file")

      # jika namanya cocok
      if [ -n "$matched_line" ]; then
          # ngambil data dari baris file csv yang cocok
          IFS=',' read -r char_name region elemen senjata <<< "$matched_line"

          # rename
          new_filename="${char_name} - ${region} - ${elemen} - ${senjata}.jpg"

          # rename file kalo nama yang baru gak kosong
          if [ -n "$new_filename" ]; then
              mv "$file" "$new_filename"
              echo "Rename file: $filename -> $new_filename"
          fi
      else
          echo "Tidak ada cocok dalam CSV untuk file: $filename"
      fi
  done
}

rename_files

# ngehapus karakter carriage return
for renamed_file in *.jpg; do
    new_name=$(basename "$renamed_file" | tr -d $'\r')
    mv "$renamed_file" "$new_name"
    echo "Hapus karakter carriage return: $renamed_file -> $new_name"
done

mkdir -p "Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine"

# mindahin semua file sesuai sama direktorinya
for file in *.jpg; do
    # ngambil nama chara dari nama file
    char_name=$(basename "$file" | cut -d ' ' -f 1)

    # nyari direktori
    target_dir=""
    if grep -q "$char_name" "$csv_file"; then
        target_dir=$(grep "$char_name" "$csv_file" | cut -d ',' -f 2)
    fi

    if [ -n "$target_dir" ] && [ -d "$target_dir" ]; then
        mv "$file" "$target_dir/$(basename "$file")"
        echo "Pindahkan file ke direktori $target_dir: $file"
    else
        echo "Tidak ada direktori yang sesuai atau karakter tidak ditemukan dalam CSV untuk file: $file"
    fi
done

declare -A senjata_counts

# ngecek dari file csv & ngitung jumlah pengguna senjatanya
while IFS=',' read -r char_name region elemen senjata; do
    if [ -n "$senjata" ]; then
        jenis_senjata=""
        case "$senjata" in
            *Catalyst*) jenis_senjata="Catalyst" ;;
            *Polearm*) jenis_senjata="Polearm" ;;
            *Claymore*) jenis_senjata="Claymore" ;;
            *Sword*) jenis_senjata="Sword" ;;
            *Bow*) jenis_senjata="Bow" ;;
        esac

        if [ -n "$jenis_senjata" ]; then
            senjata_counts["$jenis_senjata"]=$((senjata_counts["$jenis_senjata"] + 1))
        fi
    fi
done < "$csv_file"

echo "Jumlah pengguna untuk setiap jenis senjata:"
for senjata in "${!senjata_counts[@]}"; do
    count="${senjata_counts["$senjata"]}"
    echo "$senjata: $count"
done

rm genshin_character.zip list_character.csv genshin.zip
