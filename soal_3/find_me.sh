#!/bin/bash

# direktori yang dipake
directories=("Mondstat" "Liyue" "Inazuma" "Fontaine" "Sumeru")

# loop buat ngecek semua direktori
for directory in "${directories[@]}"; do
    echo "Mengecek direktori: $directory"
    
    # loop ke semua file gambar di direktori yang dipake
    for gambar in "$directory"/*.jpg; do
        if [ -f "$gambar" ]; then
            echo "Mengekstrak pesan tersembunyi dari: $gambar"
            
            # ekstrak pesan tersembunyi
            steghide extract -sf "$gambar"
            
            # ngecek proses ekstraknya berhasil atau ngga
            if [ $? -eq 0 ]; then
                echo "Pesan tersembunyi berhasil diekstrak dari $gambar"
                
                # loop ke semua file txt
                for txt_file in "$directory"/*.txt; do
                    if [ -f "$txt_file" ]; then
                        # dekrip semua isi file
                        base64 -d "$txt_file" | cat
                    fi
                done
            else
                echo "Tidak ada pesan tersembunyi yang ditemukan atau kesalahan saat mengekstrak dari $gambar"
            fi
        fi
    done
done
