while true; do
    current_time=$(date "+%Y%m%d%H%M%S")
    ram_info=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7","$8}')
    swap_info=$(free -m | awk 'NR==3 {print $2","$3","$4}')
    target_path="/home/areuka/no4/"
    path_size=$(du -sh $target_path | awk '{print $1}')
    log_entry="$ram_info,$swap_info,$target_path,$path_size"
    echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$log_entry" >> "/home/areuka/no4/log/metrics_${current_time}.log"
    sleep 60 
done

chown areuka:areuka /home/areuka/no4/log/metrics_${current_time}.log
chmod 600 /home/areuka/no4/log/metrics_${current_time}.log

#CRONJOB
#* * * * * /bin/bash /home/areuka/no4/minute_log.sh