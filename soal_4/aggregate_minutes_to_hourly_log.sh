#!/bin/bash

current_hour=$(date "+%Y%m%d%H")

minute_logs=$(find /home/areuka/no4/log/ -type f -name "metrics_${current_hour}*.log")

mem_total_values=()
mem_used_values=()
mem_free_values=()
mem_shared_values=()
mem_buff_values=()
mem_available_values=()
swap_total_values=()
swap_used_values=()
swap_free_values=()
path_values=()
path_size_values=()

for log in $minute_logs; do
    metrics=($(awk -F, 'NR>1 {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14}' "$log"))  # Mengabaikan header dan hanya mengambil kolom yang diperlukan
    mem_total_values+=("${metrics[0]}")
    mem_used_values+=("${metrics[1]}")
    mem_free_values+=("${metrics[2]}")
    mem_shared_values+=("${metrics[3]}")
    mem_buff_values+=("${metrics[4]}")
    mem_available_values+=("${metrics[5]}")
    swap_total_values+=("${metrics[6]}")
    swap_used_values+=("${metrics[7]}")
    swap_free_values+=("${metrics[8]}")
    path_values+=("${metrics[9]}")
    path_size_values+=("${metrics[10]}")
done

min_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | sort -n | head -n 1)
max_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | sort -n | tail -n 1)
avg_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | sort -n | head -n 1)
max_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | sort -n | tail -n 1)
avg_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | sort -n | head -n 1)
max_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | sort -n | tail -n 1)
avg_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | sort -n | head -n 1)
max_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | sort -n | tail -n 1)
avg_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | sort -n | head -n 1)
max_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | sort -n | tail -n 1)
avg_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | sort -n | head -n 1)
max_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | sort -n | tail -n 1)
avg_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | sort -n | head -n 1)
max_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | sort -n | tail -n 1)
avg_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | sort -n | head -n 1)
max_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | sort -n | tail -n 1)
avg_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | sort -n | head -n 1)
max_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | sort -n | tail -n 1)
avg_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | sort -n | head -n 1)
max_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | sort -n | tail -n 1)
avg_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

agg_file="/home/areuka/no4/log/metrics_agg_${current_hour}.log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "$agg_file"
echo "minimum,$min_mem_total,$min_mem_used,$min_mem_free,$min_mem_shared,$min_mem_buff,$min_mem_available,$min_swap_total,$min_swap_used,$min_swap_free,${path_values[0]},$min_path_size" >> "$agg_file"
echo "maximum,$max_mem_total,$max_mem_used,$max_mem_free,$max_mem_shared,$max_mem_buff,$max_mem_available,$max_swap_total,$max_swap_used,$max_swap_free,${path_values[0]},$max_path_size" >> "$agg_file"
echo "average,$avg_mem_total,$avg_mem_used,$avg_mem_free,$avg_mem_shared,$avg_mem_buff,$avg_mem_available,$avg_swap_total,$avg_swap_used,$avg_swap_free,${path_values[0]},$avg_path_size" >> "$agg_file"

chown areuka:areuka /home/areuka/no4/log/metrics_agg_${current_hour}.log
chmod 600 /home/areuka/no4/log/metrics_agg_${current_hour}.log

#CRONJOB
#0 * * * * /bin/bash /home/areuka/no4/aggregate_minutes_to_hourly_log.sh

