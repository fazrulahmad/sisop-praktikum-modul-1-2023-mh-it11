#!/bin/bash

read -p "Enter email    : " email
read -s -p "Enter password : " password

encryptPwd=" Password : $(echo -n "$password" | base64)"

if grep -q "$email" ./users/users.txt
then
  usrname=$(grep "$email" ./users/users.txt | cut -d, -f 1 | cut -d ' ' -f 3)
  passwrd=$(grep "$email" ./users/users.txt | cut -d, -f 3)
  if [ "$encryptPwd" == "$passwrd" ]
  then
    echo -e "\n\nLOGIN SUCCESS - Welcome, $usrname"
    echo "[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN SUCCESS] user $usrname login attempt successfully" >> auth.log
  else
    echo -e "\n\nLOGIN FAILED - Email and password doesn't match"
    echo "[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> auth.log
  fi
else
  echo -e "\n\nLOGIN FAILED - email $email not registered, please register first"
  echo "\n[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> auth.log
fi