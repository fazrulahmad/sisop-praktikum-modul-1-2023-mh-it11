# Kelompok IT11 Sistem Operasi

Kelompok IT11 :
| Nama | NRP |
| -------------------------------------- | ---------- |
| Mohammad Arkananta Radithya Taratugang | 5027221003 |
| Fazrul Ahmad Fadhilah | 5027221025 |
| Samuel Yuma Krismata | 5027221029 |

---

## Soal 1

Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.

### Deskripsi

Membuat script dengan nama playlist_keren.sh untuk menampilkan data yang telah di-filter dari sebuah file yang berformat.csv

### Solusi

Untuk menyelesaikan permasalahan tersebut, Farrel perlu mengunduh file `playlist.csv` terlebih dahulu.

### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
wget --no-check-certificate "https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp" -O "top_lagu_milik_farrel.csv"
```

**Penjelasan** </br>

1. `wget --no-check-certificate "https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp"` : Mengunduh file dari google drive dengan mengabaikan pemeriksaan sertifikat SSL agar tidak terjadi error sangat mengunduh file.<br>
2. `-O "top_lagu_milik_farrel.csv"` : Mengkonfigurasi nama file hasil unduhan menjadi `top_lagu_milik_farrel.csv`<br>

### Output

![1](/uploads/3cd0275f72531ae0e5d7359536a11b83/1.png)

### Poin A

#### Deskripsi

Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Farrel perlu melakukan filter terhadap data yang tersedia dengan memfilter menggunakan kata kunci `hip hop` dan mengurutkan data yang ada dari yang terbesar ke terkecil berdasarkan kolom `popularity`. Selanjutnya, Farrel menampilkan 5 data teratas yang merupakan hasil dari filter tersebut.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
echo -e "5 top lagu teratas dengan genre hip hop:"
grep -i 'hip hop' top_lagu_milik_farrel.csv | sort -t ',' -k15,15nr | head -n 5
```

**Penjelasan** </br>

1. `echo "5 top lagu teratas dengan genre hip hop:"` : Menampilkan pesan yang berada dalam tanda kutip ke dalam konsol.<br>
2. `grep -i 'hip hop' top_lagu_milik_farrel.csv` : Mencari kata `hip hop` dalam file `top_lagu_milik_farrel.csv` dengan pencarian yang bersifat case-insensitive sehingga kata `hip hop` dapat ditemukan dalam berbagai huruf baik besar maupun kecil.<br>
3. `| sort -t ',' -k15,15nr` : Mengurutkan data dengan delimiter berupa tanda `,` berdasarkan kolom ke-15 dengan urutan data secara descending (terbesar ke terkecil).<br>
4. `| head -n 5` : Memilih 5 data yang teratas dari hasil filter.<br>

#### Output

![1a](/uploads/ccab5d120efc23d03618a2a8c9c357f5/1a.png)

### Poin B

#### Deskripsi

Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Farrel perlu melakukan filter terhadap data yang tersedia dengan memfilter menggunakan kata kunci `John Mayer` dan mengurutkan data yang ada dari yang terkecil ke terbesar berdasarkan kolom `popularity`. Selanjutnya, Farrel menampilkan 5 data teratas yang merupakan hasil dari filter tersebut.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
echo -e "\n5 lagu terbawah John Mayer:"
grep -i 'John Mayer' top_lagu_milik_farrel.csv | sort -t ',' -k15,15n | head -n 5
```

**Penjelasan** </br>

1. `echo -e "\n5 lagu terbawah John Mayer:"` : Menampilkan pesan yang berada dalam tanda kutip ke dalam konsol.<br>
2. `grep -i 'John Mayer' top_lagu_milik_farrel.csv` : Mencari kata `John Mayer` dalam file `top_lagu_milik_farrel.csv` dengan pencarian yang bersifat case-insensitive sehingga kata `John Mayer` dapat ditemukan dalam berbagai huruf baik besar maupun kecil.<br>
3. `| sort -t ',' -k15,15n` : Mengurutkan data dengan delimiter berupa tanda `,` berdasarkan kolom ke-15 dengan urutan data secara ascending (terkecil ke terbesar).<br>
4. `| head -n 5` : Memilih 5 data yang teratas dari hasil filter.<br>

#### Output

![1b](/uploads/a1179a316bfe8f53b92a156a88e492c0/1b.png)

### Poin C

#### Deskripsi

Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Farrel perlu melakukan filter terhadap data yang tersedia dengan memfilter menggunakan kata kunci `2004` dan mengurutkan data yang ada dari yang terkecil ke terbesar berdasarkan kolom `popularity`. Selanjutnya, Farrel menampilkan 10 data teratas yang merupakan hasil dari filter tersebut.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
echo -e "\n10 lagu teratas tahun 2004:"
grep -i '2004' top_lagu_milik_farrel.csv | sort -t ',' -k15,15n | head -n 10
```

**Penjelasan** </br>

1. `echo -e "\n10 lagu teratas tahun 2004:"` : Menampilkan pesan yang berada dalam tanda kutip ke dalam konsol.<br>
2. `grep -i '2004' top_lagu_milik_farrel.csv` : Mencari kata `2004` dalam file `top_lagu_milik_farrel.csv`.<br>
3. `| sort -t ',' -k15,15n` : Mengurutkan data dengan delimiter berupa tanda `,` berdasarkan kolom ke-15 dengan urutan data secara ascending (terkecil ke terbesar).<br>
4. `| head -n 10` : Memilih 10 data yang teratas dari hasil filter.<br>

#### Output

![1c](/uploads/4e8ba6137bfce686bb9043dadbc216c5/1c.png)

### Poin D

#### Deskripsi

Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Farrel perlu melakukan filter terhadap data yang tersedia dengan memfilter menggunakan kata kunci `sri`. Selanjutnya, Farrel menampilkan data yang merupakan hasil dari filter tersebut.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
echo -e "\nLagu ciptaan ibu Sri:"
grep -i 'sri' top_lagu_milik_farrel.csv
```

**Penjelasan** </br>

1. `echo -e "\nLagu ciptaan ibu Sri:"` : Menampilkan pesan yang berada dalam tanda kutip ke dalam konsol.<br>
2. `grep -i 'sri' top_lagu_milik_farrel.csv` : Mencari kata `sri` dalam file `top_lagu_milik_farrel.csv` dengan pencarian yang bersifat case-insensitive sehingga kata `sri` dapat ditemukan dalam berbagai huruf baik besar maupun kecil.<br>

#### Ouput

![1d](/uploads/54ae1b1b8df2d73dc8807907bb042093/1d.png)

## Soal 2

Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

### Poin A

#### Deskripsi

Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Shinichi harus membuat sebuah file yang berisikan script untuk menyimpan nama user dan email.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
read -p "Masukkan username baru : " username
read -p "Masukkan email baru    : " email
```

**Penjelasan** </br>

1. `read -p "Masukkan username baru : " username` : Menampilkan teks yang ada dalam tanda petik, lalu meminta input dari pengguna yang kemudian disimpan dalam variabel `username`.<br>
2. `read -p "Masukkan email baru    : " email` : Menampilkan teks yang ada dalam tanda petik, lalu meminta input dari pengguna yang kemudian disimpan dalam variabel `email`.<br>

### Poin B

#### Deskripsi

Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.

- Password tersebut harus di encrypt menggunakan base64
- Password yang dibuat harus lebih dari 8 karakter
- Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
- Password tidak boleh sama dengan username
- Harus terdapat paling sedikit 1 angka
- Harus terdapat paling sedikit 1 simbol unik

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Shinichi harus mengatur agar password yang dimasukkan tidak melanggar aturan yang telah ditentukan dengan menggunakan conditional statement yaitu `if else`.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
read -s -p "Masukkan password baru : " password

if [ ${#password} -le 8 ]
then
    echo -e "\n\nREGISTER FAILED - Password yang dibuat harus lebih dari 8 karakter"
    echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]]
then
    echo -e "\n\nREGISTER FAILED - Password harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil"
    echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif [ "$password" = "$username" ]
then
    echo -e "\n\nREGISTER FAILED - Password tidak boleh sama dengan username"
    echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif ! [[ "$password" =~ [0-9] ]]
then
    echo -e "\n\nREGISTER FAILED - Password harus terdapat paling tidak 1 angka"
    echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif ! [[ "$password" =~ [!@#$%\^\&*()] ]]
then
    echo -e "\n\nREGISTER FAILED - Password harus terdapat paling tidak 1 simbol unik [ !@#$%^&*() ]"
    echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
else
    encryptPass=$(echo -n "$password" | base64)
```

**Penjelasan** </br>

1. `read -s -p "Masukkan password baru : " password` : Menampilkan teks yang ada dalam tanda petik, lalu meminta input dari pengguna yang kemudian disimpan dalam variabel `password`.<br>
2. `if [ ${#password} -le 8 ]` : Mengecek apakah karakter dari password yang dimasukkan oleh user kurang dari delapan atau tidak.<br>
3. `elif ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]]` : Mengecek apakah password yang dimasukkan oleh user terdapat huruf besar/huruf kecil. atau tidak.<br>
4. `elif [ "$password" = "$username" ]` : Mengecek apakah password yang dimasukkan oleh user sama dengan username atau tidak.<br>
5. `elif ! [[ "$password" =~ [0-9] ]]` : Mengecek apakah password yang dikasukkan oleh user terdapat angka atau tidak.<br>
6. `elif ! [[ "$password" =~ [!@#$%\^\&*()] ]]` : Mengecek apakah password yang dimasukkan oleh user terdapat simbol atau tidak. Dalam case ini, simbol yang dapat dipakai hanya simbol `!@#$%^&*()` saja.<br>
7. `else  encryptPass=$(echo -n "$password" | base64)` : Jika password yang dimasukkan oleh user memenuhi semua kriteria, maka password tersebut akan di encrypt menggunakan `base64`.<br>

#### Output

![2b](/uploads/46786434f6850e4a2b58acd7320cc6b9/2b.png)

### Poin C

#### Deskripsi

Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Shinichi harus membuat folder baru yang bernama `users` dan membuat file bernama `users.txt` didalamnya, lalu memasukkan setiap username, email dan password yang diinputkan pada script `register.sh` ke dalam file `users.txt` tersebut.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
echo "Username : $username, Email : $email, Password : $encryptPass" >> users.txt
```

**Penjelasan** </br>
Menyimpan username, email serta password yang telah dienkripsi ke dalam file bernama `users.txt`.

### Poin D

#### Deskripsi

Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal.

#### Solusi

Untuk menyelesaikan permasalahan ini, Shinichi perlu menampilkan pesan pada setiap case yang ada di conditional statement `if else`.

#### Code

Berikut adalah contoh salah satu case pada conditional statement untuk menampilkan bahwa login berhasil atau gagal:

```bash
if [ ${#password} -le 8 ]
then
    echo -e "\n\nREGISTER FAILED - Password yang dibuat harus lebih dari 8 karakter"
```

**Penjelasan** <br>
Apabila karakter dari password yang dimasukkan oleh user kurang dari delapan, maka pesan `REGISTER FAILED - Password yang dibuat harus lebih dari 8 karakter` akan ditampilkan.

### Poin E

#### Deskripsi

Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Shinichi membuat sebuah file script dengan nama `login.sh` yang didalamnya terdapat input untuk memasukkan email dan password.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
read -p "Enter email    : " email
read -s -p "Enter password : " password
```

**Penjelasan** </br>

1. `read -p "Enter email    : " email` : Menampilkan teks yang ada dalam tanda petik, lalu meminta input dari pengguna yang kemudian disimpan dalam variabel `email`.<br>
2. `read -s -p "Enter password : " password` : Menampilkan teks yang ada dalam tanda petik, lalu meminta input dari pengguna yang kemudian disimpan dalam variabel `password`.<br>

### Poin F

#### Deskripsi

Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.
**Ex**:

- LOGIN SUCCESS - Welcome, [username]
- LOGIN FAILED - email [email] not registered, please register first

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Shinichi harus mengatur agar password dan email yang dimasukkan user sesuai dengan apa yang ada dalam file `users.txt` dengan menggunakan conditional statement yaitu `if else` serta menampilkan respon baik saat login gagal maupun saat login berhasil.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
encryptPwd=" Password : $(echo -n "$password" | base64)"

if grep -q "$email" ./users/users.txt
then
  usrname=$(grep "$email" ./users/users.txt | cut -d, -f 1 | cut -d ' ' -f 3)
  passwrd=$(grep "$email" ./users/users.txt | cut -d, -f 3)
  if [ "$encryptPwd" == "$passwrd" ]
  then
    echo -e "\n\nLOGIN SUCCESS - Welcome, $usrname"
    echo "[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN SUCCESS] user $usrname login attempt successfully" >> auth.log
  else
    echo -e "\n\nLOGIN FAILED - Email and password doesn't match"
    echo "[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> auth.log
  fi
else
  echo -e "\n\nLOGIN FAILED - email $email not registered, please register first"
  echo "\n[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> auth.log
fi
```

**Penjelasan** </br>

1. `encryptPwd=" Password : $(echo -n "$password" | base64)"` : Mengenkripsi password yang telah diinputkan user menggunakan base64.</br>
2. `usrname=$(grep "$email" ./users/users.txt | cut -d, -f 1 | cut -d ' ' -f 3)`: Mengambil karakter email yang ada dalam file `users.txt`.</br>
3. `passwrd=$(grep "$email" ./users/users.txt | cut -d, -f 3)` : Mengambil karakter password yang ada dalam file `users.txt`.</br>
4. `if grep -q "$email" ./users/users.txt` : Mengecek apakah email yang diinputkan user terdapat dalam file `users.txt`.</br>
5. `else  echo -e "\n\nLOGIN FAILED - email $email not registered, please register first"` : Apabila email tidak terdapat dalam file `users.txt`, maka pesan `LOGIN FAILED - email $email not registered, please register first` akan ditampilkan.</br>
6. `if [ "$encryptPwd" == "$passwrd" ]` : Mengecek apakah password yang diinputkan user dan dienkripsi menggunakan base64 sama dengan password yang telah dienkripsi yang terdapat dalam file `users.txt`. </br>

#### Output

![2f](/uploads/5e1a5519580883a7fbcd5b7077ff608c/2f.png)

### Poin G

#### Deskripsi

Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.
**Format**: [date] [type] [message]
**Type**: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
**Ex**:

- [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
- [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]

#### Solusi

Untuk menyelesaikan permasalahan tersebut, Shinichi harus membuat sebuah file bernama `auth.log` dan mencatat setiap log dari login maupun register ke dalam file `auth.log` tersebut.

#### Code

Berikut adalah salah satu contoh kode yang merupakan implementasi dari permasalahan tersebut:

```bash
# register.sh

if [ ${#password} -le 8 ]
then
    echo -e "\n\nREGISTER FAILED - Password yang dibuat harus lebih dari 8 karakter"
    echo "[ $(date +'%m/%d/%Y') $(date +'%T') ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
```

```bash
# login.sh

if [ "$encryptPwd" == "$passwrd" ]
  then
    echo -e "\n\nLOGIN SUCCESS - Welcome, $usrname"
    echo "[ $(date +'%d/%m/%Y') $(date +'%T') ] [LOGIN SUCCESS] user $usrname login attempt successfully" >> auth.log
```

**Penjelasan** </br>

1. `$(date +'%d/%m/%Y')` : Mengambil value `hari, bulan, dan tahun` saat log tersebut dibuat.</br>
2. `$(date +'%T')` : Mengambil value `waktu` saat log tersebut dibuat.</br>
3. Setelah case memenuhi kondisi tertentu, maka pesan akan ditampilkan dan log akan dicatat serta masuk ke dalam file `auth.log`.</br>

#### Output

![2g](/uploads/ebb896efdcedbf42bd831109cbf76572/2g.png)

## Soal 3

Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.

### Poin A

#### Deskripsi

Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
i. **Format**: Nama - Region - Elemen - Senjata.jpg

#### Solusi
Pertama-tama buatlah script bash bernama `genshin.sh` untuk menjalankan command yang diperlukan. Lalu downloadlah link yang ada pada soal menggunakan command `wget`. Setelah file drive terdownload, lakukan unzip pada file drive yang telah didownload dan unzip file yang ada di dalamnya menggunakan command `unzip`. Didapatkanlah isi dari file `genshin_character.zip` dan rename setiap filenya sesuai dengan file `list_character.csv`. Setelah direname, terdapat karakter non-printable dan carriage return pada nama filenya, saya membuat command tambahan untuk menghilangkan karakter non-printable dan carriage return. Lalu selanjutnya buatlah direktori dengan nama dari region yang ada dan pindahkan setiap file ke direktori yang sesuai dengan regionnya.

#### Code
```bash
#!/bin/bash

wget --no-check-certificate "https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2" -O genshin.zip
unzip genshin.zip
unzip genshin_character.zip

# Deklarasikan variabel dengan nama file CSV dan direktori
csv_file="list_character.csv"
directory="praktikumsisop"

# Pastikan file CSV ada
if [ ! -f "$csv_file" ]; then
    echo "File CSV tidak ditemukan: $csv_file"
    exit 1
fi

# Hapus semua karakter non-printable dari nama file
rename_files() {
  for file in *.jpg; do
      # Ambil nama file (tanpa path)
      filename=$(basename "$file")

      # Hapus semua karakter non-printable
      cleaned_name=$(echo "$filename" | tr -cd '[:print:]')

      # Decode nama file dari Base64
      decoded_name=$(echo "$cleaned_name" | base64 -d)

      # Cari nama yang cocok dalam file CSV
      matched_line=$(grep "$decoded_name" "$csv_file")

      # Jika nama cocok dalam CSV, proses file
      if [ -n "$matched_line" ]; then
          # Ambil data dari baris CSV yang cocok
          IFS=',' read -r char_name region elemen senjata <<< "$matched_line"

          # Buat nama baru
          new_filename="${char_name} - ${region} - ${elemen} - ${senjata}.jpg"

          # Rename file jika nama baru tidak kosong
          if [ -n "$new_filename" ]; then
              mv "$file" "$new_filename"
              echo "Rename file: $filename -> $new_filename"
          fi
      else
          echo "Tidak ada cocok dalam CSV untuk file: $filename"
      fi
  done
}

# Panggil fungsi untuk mengganti nama file dalam direktori
rename_files

# Hapus karakter carriage return dari nama file
for renamed_file in *.jpg; do
    new_name=$(basename "$renamed_file" | tr -d $'\r')
    mv "$renamed_file" "$new_name"
    echo "Hapus karakter carriage return: $renamed_file -> $new_name"
done

# Buat direktori jika belum ada
mkdir -p "Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine"

# Pindahkan semua file ke direktorinya masing-masing berdasarkan nama
for file in *.jpg; do
    # Ambil nama karakter dari nama file
    char_name=$(basename "$file" | cut -d ' ' -f 1)

    # Cari direktori yang sesuai dengan karakter
    target_dir=""
    if grep -q "$char_name" "$csv_file"; then
        target_dir=$(grep "$char_name" "$csv_file" | cut -d ',' -f 2)
    fi

    if [ -n "$target_dir" ] && [ -d "$target_dir" ]; then
        mv "$file" "$target_dir/$(basename "$file")"
        echo "Pindahkan file ke direktori $target_dir: $file"
    else
        echo "Tidak ada direktori yang sesuai atau karakter tidak ditemukan dalam CSV untuk file: $file"
    fi
done
```

**Penjelasan** <br>
`wget --no-check-certificate "https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2" -O genshin.zip` : mendownload link pada soal </br>
`unzip genshin.zip` : mengunzip file drive dari soal </br>
`unzip genshin_character.zip` : mengunzip file yang ada di dalam drive </br>
```bash
if [ ! -f "$csv_file" ]; then
    echo "File CSV tidak ditemukan: $csv_file"
    exit 1
fi
```
memastikan file csv ada </br>
`for file in *.jpg;` : melakukan looping untuk mengecek semua file .jpg di direktori sekarang </br>
`filename=$(basename "$file")` : mengambil nama file tanpa path </br>
`cleaned_name=$(echo "$filename" | tr -cd '[:print:]')` : menghapus semua karakter non-printable </br>
`decoded_name=$(echo "$cleaned_name" | base64 -d)` : mendekode setiap nama file menggunakan base64 </br>
`matched_line=$(grep "$decoded_name" "$csv_file")` : mencocokkan nama file dengan yang ada di list_character.csv </br>
`if [ -n "$matched_line" ];` : melakukan perulangan jika namanya cocok dengan yang ada di file csv </br>
`IFS=',' read -r char_name region elemen senjata <<< "$matched_line"` : mengambil data dari baris CSV yang cocok </br>
`new_filename="${char_name} - ${region} - ${elemen} - ${senjata}.jpg"` : membuat nama file yang baru </br>
```bash
for renamed_file in *.jpg; do
    new_name=$(basename "$renamed_file" | tr -d $'\r')
    mv "$renamed_file" "$new_name"
    echo "Hapus karakter carriage return: $renamed_file -> $new_name"
done
```
Untuk menghapus karakter carriage return </br>
`mkdir -p "Mondstat" "Liyue" "Inazuma" "Sumeru" "Fontaine"` : membuat direktori dengan nama region yang ada </br>
`for file in *.jpg; do` : melakukan looping pada semua file yang telah direname pada direktori sekarang </br>
`char_name=$(basename "$file" | cut -d ' ' -f 1)` : mengambil nama character dari nama file </br>
```bash
target_dir=""
    if grep -q "$char_name" "$csv_file"; then
        target_dir=$(grep "$char_name" "$csv_file" | cut -d ',' -f 2)
    fi

    if [ -n "$target_dir" ] && [ -d "$target_dir" ]; then
        mv "$file" "$target_dir/$(basename "$file")"
        echo "Pindahkan file ke direktori $target_dir: $file"
    else
        echo "Tidak ada direktori yang sesuai atau karakter tidak ditemukan dalam CSV untuk file: $file"
    fi
done
```
Mencari direktori yang sesuai dengan character dan memindahkan filenya. </br>
### Poin B

#### Deskripsi

Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
i. **Format**: [Nama Senjata] : [total]
Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip

#### Solusi
Untuk menyelesaikan soal ini kita memerlukan fungsi `if` untuk mengecek nama senjata dan fungsi `for` untuk menampilkan jumlah pengguna setiap senjata yang ada. Terakhir menggunakan command `rm` untuk menghapus file-file yang sudah tidak diperlukan. 

#### Code
```bash
# Mendeklarasikan variabel senjata dan inisialisasi dengan array kosong
declare -A senjata_counts

# Membaca data senjata dari file CSV dan menghitung jumlah pengguna untuk setiap senjata
while IFS=',' read -r char_name region elemen senjata; do
    if [ -n "$senjata" ]; then
        # Mengambil jenis senjata (Catalyst, Polearm, Claymore, Sword, Bow)
        jenis_senjata=""
        case "$senjata" in
            *Catalyst*) jenis_senjata="Catalyst" ;;
            *Polearm*) jenis_senjata="Polearm" ;;
            *Claymore*) jenis_senjata="Claymore" ;;
            *Sword*) jenis_senjata="Sword" ;;
            *Bow*) jenis_senjata="Bow" ;;
        esac

        if [ -n "$jenis_senjata" ]; then
            senjata_counts["$jenis_senjata"]=$((senjata_counts["$jenis_senjata"] + 1))
        fi
    fi
done < "$csv_file"

# Menampilkan jumlah pengguna untuk setiap jenis senjata
echo "Jumlah pengguna untuk setiap jenis senjata:"
for senjata in "${!senjata_counts[@]}"; do
    count="${senjata_counts["$senjata"]}"
    echo "$senjata: $count"
done

rm genshin_character.zip list_character.csv genshin.zip
```
**Penjelasan** <br>
`declare -A senjata_counts` : mendeklarasikan variabel senjata dan inisiasi dengan array kosong. </br>
```bash
while IFS=',' read -r char_name region elemen senjata; do
    if [ -n "$senjata" ]; then
        # Mengambil jenis senjata (Catalyst, Polearm, Claymore, Sword, Bow)
        jenis_senjata=""
        case "$senjata" in
            *Catalyst*) jenis_senjata="Catalyst" ;;
            *Polearm*) jenis_senjata="Polearm" ;;
            *Claymore*) jenis_senjata="Claymore" ;;
            *Sword*) jenis_senjata="Sword" ;;
            *Bow*) jenis_senjata="Bow" ;;
        esac

        if [ -n "$jenis_senjata" ]; then
            senjata_counts["$jenis_senjata"]=$((senjata_counts["$jenis_senjata"] + 1))
        fi
    fi
done < "$csv_file"
```
Script di atas memiliki fungsi untuk membaca data senjata daru file CSV dan menghitung jumlah penggunanya untuk setiap senjata. </br>

```bash
echo "Jumlah pengguna untuk setiap jenis senjata:"
for senjata in "${!senjata_counts[@]}"; do
    count="${senjata_counts["$senjata"]}"
    echo "$senjata: $count"
done
```
Script di atas berfungsi untuk menampilkan jumlah setiap pengguna senjata untuk senjata yang ada. </br>

`rm genshin_character.zip list_character.csv genshin.zip` : menghapus file `genshin_character.zip`, `list_character.zip`, dan `genshin.zip`. </br>

### Poin C

#### Deskripsi

Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.

#### Code
```bash
#!/bin/bash

# Daftar direktori tempat gambar tersembunyi disimpan
directories=("Mondstat" "Liyue" "Inazuma" "Fontaine" "Sumeru")

# Loop melalui semua direktori dalam daftar
for directory in "${directories[@]}"; do
    echo "Mengecek direktori: $directory"

    # Loop melalui semua file gambar dalam direktori gambar tersembunyi
    for gambar in "$directory"/*.jpg; do
        if [ -f "$gambar" ]; then
            echo "Mengekstrak pesan tersembunyi dari: $gambar"

            # Mengekstrak pesan tersembunyi menggunakan steghide
            steghide extract -sf "$gambar"

            # Periksa apakah proses ekstraksi berhasil
            if [ $? -eq 0 ]; then
                echo "Pesan tersembunyi berhasil diekstrak dari $gambar"

                # Loop melalui semua file teks dalam direktori saat ini
                for txt_file in "$directory"/*.txt; do
                    if [ -f "$txt_file" ]; then
                        # Mendekripsi isi file teks dengan Base64 dan menampilkannya di layar
                        base64 -d "$txt_file" | cat
                    fi
                done
            else
                echo "Tidak ada pesan tersembunyi yang ditemukan atau kesalahan saat mengekstrak dari $gambar"
            fi
        fi
    done
done
```

### Poin D

#### Deskripsi

Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
i. **Format**: [date] [type] [image_path]
ii. **Ex**: - [23/09/11 17:57:51] [NOT FOUND] [image_path] - [23/09/11 17:57:52] [FOUND] [image_path]

### Poin E

#### Deskripsi

Hasil akhir:

- genshin_character
- find_me.sh
- genshin.sh
- image.log
- [filename].txt
- [image].jpg

## Soal 4

Buatlah program monitoring resource pada laptop kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

### Poin A

#### Deskripsi

Masukkan semua metrics ke dalam suatu file log bernama metrics\_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2023-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20230131150000.log.

#### Solusi

Untuk menyelesaikan permasalahan tersebut, perlu dibuat script bash minute_log.sh yang berisi command `free-m` dan `du -sh <target_path>` untuk mencatat metrics. Lalu hasil disimpan pada file log bernama metrics_{YmdHms}.log dengan cara menyimpan data waktu file tersebut dijalankan menggunakan `date "+%Y%m%d%H%M%S"`

### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

```bash
while true; do
    current_time=$(date "+%Y%m%d%H%M%S")
    ram_info=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7","$8}')
    swap_info=$(free -m | awk 'NR==3 {print $2","$3","$4}')
    target_path="/home/areuka/no4/"
    path_size=$(du -sh $target_path | awk '{print $1}')
    log_entry="$ram_info,$swap_info,$target_path,$path_size"
    echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$log_entry" >> "/home/areuka/no4/log/metrics_${current_time}.log"
done
```

**Penjelasan** <br>

1. `while true; do` ini adalah awal dari loop tak terbatas. Loop akan terus berjalan selama kondisi `true` tetap terpenuhi, yang berarti loop akan berjalan tanpa henti sampai dipaksa untuk berhenti
2. `current_time=$(date "+%Y%m%d%H%M%S")` Ini mendefinisikan variabel `current_time` yang akan menyimpan tanggal dan waktu saat ini dalam format yang disesuaikan (YYYYMMDDHHMMSS).
3. `ram_info=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7","$8}')` Ini mengumpulkan informasi tentang penggunaan RAM (memori fisik). Perintah `free -m` digunakan untuk mendapatkan informasi ini, dan outputnya diolah oleh `awk` untuk mengambil data pada baris kedua (NR==2) yang berisi informasi tentang RAM. Data ini kemudian dipecah menjadi beberapa kolom (total, used, free, shared, buff, available,swap).
4. `swap_info=$(free -m | awk 'NR==3 {print $2","$3","$4}')` Ini mengumpulkan informasi tentang penggunaan swap (memori virtual). Sama seperti sebelumnya, perintah `free -m` digunakan untuk mendapatkan informasi ini, dan outputnya diolah oleh `awk` untuk mengambil data pada baris ketiga (NR==3) yang berisi informasi tentang swap. Data ini juga dipecah menjadi tiga kolom (total, used, free).
5. `target_path="/home/areuka/no4/"`Variabel target_path diatur ke direktor yang ingin diukur.
6. `path_size=$(du -sh $target_path | awk '{print $1}')` Ini menghitung ukuran total dari direktori target yang telah ditetapkan dalam variabel  `target_path` menggunakan perintah `du -sh`. Hasilnya diambil dengan bantuan `awk` dan disimpan dalam variabel `path_size`.
7. `log_entry="$ram_info,$swap_info,$target_path,$path_size"` Ini menggabungkan semua informasi yang telah dikumpulkan (RAM, swap, path, dan ukuran path) menjadi satu string dalam variabel `log_entry`.
8. `echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$log_entry"` Baris ini mencetak header kolom dan nilai-nilai yang telah dikumpulkan ke dalam file log dengan nama yang disusun berdasarkan timestamp saat ini. Ini dilakukan dengan menggunakan `echo -e` untuk menginterpretasikan karakter escape (seperti `\n` untuk baris baru) dan kemudian menggabungkan header kolom dan data dalam variabel `log_entry`. Hasilnya akan ditambahkan ke dalam file log dengan format "metrics_YYYYMMDDHHMMSS.log" di direktori "/home/areuka/no4/log".
9. `done` Ini menandakan akhir dari loop while. Setelah mencatat informasi ke dalam file log, skrip akan kembali ke awal loop dan menjalankan proses yang sama lagi. Ini akan terus berlangsung sampai loop dihentikan secara manual.

### Output

![image](/uploads/8d705c4f855201a7e250b9775559b203/image.png)

Gambar di atas merupakan isi dari directory `log`

![image](/uploads/0e1696503721783c0b08bdf8df28d59d/image.png)

Gambar di atas merupakan isi dari salah satu file `metrics_{YmdHms}.log`


### Poin B

#### Deskripsi

Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

#### Solusi

Digunakan pengeditan crontab untuk menjalankan script `minute_log.sh` secara terus-menerus setiap menit.

### Code

Berikut adalah kode untuk menjalankan `minute_log.sh` secara terus-menerus setiap menit
```bash
* * * * * /bin/bash /home/areuka/no4/minute_log.sh
```

**Penjelasan** <br>
1.  `* * * * *` merupakan bagian dari jadwal   waktu (cron schedule) yang menentukan kapan tugas akan dijalankan. Setiap asterisk (*) dalam bagian ini mewakili sebuah field waktu tertentu dalam urutan berikut:
    - Menit (0-59)
    - Jam (0-23)
    - Hari dalam bulan (1-31)
    - Bulan (1-12 atau dengan nama Jan, Feb, Mar, dst.)
    - Hari dalam seminggu (0-6, dengan 0 sebagai Minggu dan 6 sebagai Sabtu)

    Dalam contoh ini, dengan menggunakan lima asterisk yang semuanya adalah tanda '*', itu berarti tugas akan dijalankan setiap menit, setiap jam, setiap hari, setiap bulan, dan setiap hari dalam seminggu.
2. `bin/bash` berarti cara menjalankan file adalah dengan menggunakan argumen "bash".
3. `/home/areuka/no4/minute_log.sh` berarti path dari file yang akan dijalankan oleh crontab
Sebelumnya.
### Poin C

#### Deskripsi

Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log 

#### Solusi

Untuk menyelesaikan permasalahan tersebut, perlu dibuat script bash aggregate_minutes_to_hourly_log.sh yang berfungsi untuk menghitung nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics.

#### Code

Berikut adalah kode yang merupakan implementasi dari permasalahan tersebut:

``` bash
#!/bin/bash

current_hour=$(date "+%Y%m%d%H")

minute_logs=$(find /home/areuka/no4/log/ -type f -name "metrics_${current_hour}*.log")

mem_total_values=()
mem_used_values=()
mem_free_values=()
mem_shared_values=()
mem_buff_values=()
mem_available_values=()
swap_total_values=()
swap_used_values=()
swap_free_values=()
path_values=()
path_size_values=()

for log in $minute_logs; do
    metrics=($(awk -F, 'NR>1 {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14}' "$log"))
    mem_total_values+=("${metrics[0]}")
    mem_used_values+=("${metrics[1]}")
    mem_free_values+=("${metrics[2]}")
    mem_shared_values+=("${metrics[3]}")
    mem_buff_values+=("${metrics[4]}")
    mem_available_values+=("${metrics[5]}")
    swap_total_values+=("${metrics[6]}")
    swap_used_values+=("${metrics[7]}")
    swap_free_values+=("${metrics[8]}")
    path_values+=("${metrics[9]}")
    path_size_values+=("${metrics[10]}")
done

min_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | sort -n | head -n 1)
max_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | sort -n | tail -n 1)
avg_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | sort -n | head -n 1)
max_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | sort -n | tail -n 1)
avg_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | sort -n | head -n 1)
max_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | sort -n | tail -n 1)
avg_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | sort -n | head -n 1)
max_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | sort -n | tail -n 1)
avg_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | sort -n | head -n 1)
max_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | sort -n | tail -n 1)
avg_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | sort -n | head -n 1)
max_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | sort -n | tail -n 1)
avg_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | sort -n | head -n 1)
max_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | sort -n | tail -n 1)
avg_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | sort -n | head -n 1)
max_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | sort -n | tail -n 1)
avg_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | sort -n | head -n 1)
max_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | sort -n | tail -n 1)
avg_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

min_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | sort -n | head -n 1)
max_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | sort -n | tail -n 1)
avg_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

agg_file="/home/areuka/no4/log/metrics_agg_${current_hour}.log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > "$agg_file"
echo "minimum,$min_mem_total,$min_mem_used,$min_mem_free,$min_mem_shared,$min_mem_buff,$min_mem_available,$min_swap_total,$min_swap_used,$min_swap_free,${path_values[0]},$min_path_size" >> "$agg_file"
echo "maximum,$max_mem_total,$max_mem_used,$max_mem_free,$max_mem_shared,$max_mem_buff,$max_mem_available,$max_swap_total,$max_swap_used,$max_swap_free,${path_values[0]},$max_path_size" >> "$agg_file"
echo "average,$avg_mem_total,$avg_mem_used,$avg_mem_free,$avg_mem
```

**Penjelasan**
1. `current_hour=$(date "+%Y%m%d%H")` berfungsi untuk menginisialisasi variabel `current_hour` dengan nilai waktu saat ini dalam format "YYYYMMDDHH", di mana:

    - YYYY adalah tahun.
    - MM adalah bulan.
    - DD adalah tanggal.
    - HH adalah jam.

2. `minute_logs=$(find /home/areuka/no4/log/ -type f -name "metrics_${current_hour}*.log")` berfungsi untuk mencari semua berkas log metrik yang sesuai dengan pola "metrics_YYYYMMDDHH*.log" di direktori `/home/areuka/no4/log/`. Hasil pencarian disimpan dalam variabel `minute_logs`.

3. 
    ``` bash
    mem_total_values=()
    mem_used_values=()
    mem_free_values=()
    mem_shared_values=()
    mem_buff_values=()
    mem_available_values=()
    swap_total_values=()
    swap_used_values=()
    swap_free_values=()
    path_values=()
    path_size_values=()
    ```
    berfungsi untuk untuk mendefinisikan sejumlah variabel array yang akan digunakan untuk menyimpan nilai-nilai metrik yang diperoleh dari berkas log sistem

4. 
    ``` bash
    for log in $minute_logs; do
    metrics=($(awk -F, 'NR>1 {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14}' "$log"))
    ```

    Melakukan perulangan `for` untuk setiap berkas log yang ditemukan dalam `minute_logs`. Perulangan ini menggunakan perintah `awk` untuk membaca baris-baris dalam berkas log dan memisahkan nilai-nilai metrik yang relevan (kolom 1 hingga 14) dari setiap baris. Hasilnya disimpan dalam array `metrics`.

5.  ``` bash
    mem_total_values+=("${metrics[0]}")
    mem_used_values+=("${metrics[1]}")
    mem_free_values+=("${metrics[2]}")
    mem_shared_values+=("${metrics[3]}")
    mem_buff_values+=("${metrics[4]}")
    mem_available_values+=("${metrics[5]}")
    swap_total_values+=("${metrics[6]}")
    swap_used_values+=("${metrics[7]}")
    swap_free_values+=("${metrics[8]}")
    path_values+=("${metrics[9]}")
    path_size_values+=("${metrics[10]}")
    ```

    Setiap nilai metrik yang diperoleh dari berkas log dimasukkan ke dalam array yang sesuai (misalnya, `mem_total_values` untuk total memori, `mem_used_values` untuk memori yang digunakan, dan seterusnya).
6.  ``` bash
    min_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | sort -n | head -n 1)
    max_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | sort -n | tail -n 1)
    avg_mem_total=$(IFS=$'\n'; echo "${mem_total_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | sort -n | head -n 1)
    max_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | sort -n | tail -n 1)
    avg_mem_used=$(IFS=$'\n'; echo "${mem_used_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | sort -n | head -n 1)
    max_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | sort -n | tail -n 1)
    avg_mem_free=$(IFS=$'\n'; echo "${mem_free_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | sort -n | head -n 1)
    max_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | sort -n | tail -n 1)
    avg_mem_shared=$(IFS=$'\n'; echo "${mem_shared_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | sort -n | head -n 1)
    max_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | sort -n | tail -n 1)
    avg_mem_buff=$(IFS=$'\n'; echo "${mem_buff_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | sort -n | head -n 1)
    max_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | sort -n | tail -n 1)
    avg_mem_available=$(IFS=$'\n'; echo "${mem_available_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | sort -n | head -n 1)
    max_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | sort -n | tail -n 1)
    avg_swap_total=$(IFS=$'\n'; echo "${swap_total_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | sort -n | head -n 1)
    max_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | sort -n | tail -n 1)
    avg_swap_used=$(IFS=$'\n'; echo "${swap_used_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | sort -n | head -n 1)
    max_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | sort -n | tail -n 1)
    avg_swap_free=$(IFS=$'\n'; echo "${swap_free_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')

    min_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | sort -n | head -n 1)
    max_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | sort -n | tail -n 1)
    avg_path_size=$(IFS=$'\n'; echo "${path_size_values[*]}" | awk '{ sum += $1 } END { print sum / NR }')
    ```

    berfungsi untuk menghitung statistik dasar seperti nilai minimum, maksimum, dan rata-rata untuk setiap metrik yang telah dikumpulkan.
7. `agg_file="/home/areuka/no4/log/metrics_agg_${current_hour}.log"` berfungsi untuk menginisialisasi variabel `agg_file` dengan alamat file yang akan digunakan untuk menyimpan hasil agregasi data metrik sistem.
8.  ``` bash
    echo "minimum,$min_mem_total,$min_mem_used,$min_mem_free,$min_mem_shared,$min_mem_buff,$min_mem_available,$min_swap_total,$min_swap_used,$min_swap_free,${path_values[0]},$min_path_size" >> "$agg_file"
    echo "maximum,$max_mem_total,$max_mem_used,$max_mem_free,$max_mem_shared,$max_mem_buff,$max_mem_available,$max_swap_total,$max_swap_used,$max_swap_free,${path_values[0]},$max_path_size" >> "$agg_file"
    echo "average,$avg_mem_total,$avg_mem_used,$avg_mem_free,$avg_mem_shared,$avg_mem_buff,$avg_mem_available,$avg_swap_total,$avg_swap_used,$avg_swap_free,${path_values[0]},$avg_path_size" >> "$agg_file"
    ```

    berfungsi untuk menambahkan baris-header ke berkas agregat yang mencantumkan tipe metrik dan nama kolom.

    Skrip menambahkan tiga baris data ke berkas agregat:

    - Baris pertama mencantumkan nilai minimum untuk semua metrik.
    - Baris kedua mencantumkan nilai maksimum untuk semua metrik.
    - Baris ketiga mencantumkan nilai rata-rata untuk semua metrik

9. Perlu digunakan pengeditan crontab untuk menjalankan script aggregate_minutes_to_hourly_log.sh secara terus-menerus setiap jam
    ``` bash
    0 * * * * /bin/bash /home/areuka/no4/aggregate_minutes_to_hourly_log.sh
    ```
    `0 * * * *` merupakan bagian dari jadwal waktu (cron schedule) yang menentukan kapan tugas akan dijalankan. Dalam hal ini, "0" berarti tugas ini akan dijalankan pada menit ke-0 setiap jam. Oleh karena itu, maka script akan dieksekusi secara otomatis setiap jam.
### Output

![image](/uploads/18a9fb18c950e2101a2359d758ed02e9/image.png)

Terdapat file baru dalam direktori `log` yaitu `metrics_agg_2023092921.log`

![image](/uploads/9dc3537b39d7aec4905988778709387c/image.png)

Isi dari `metrics_agg_2023092921.log`.


### Poin D

#### Deskripsi

Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

#### Solusi

Untuk mengubah agar semua file log hanya dapat dibaca oleh user pemilik file adalah dengan cara memakai `chown` dan `chmod`

### Code

``` bash
chown areuka:areuka /home/areuka/no4/log/metrics_${current_time}.log

chown areuka:areuka /home/areuka/no4/log/metrics_agg_${current_hour}.log
```
**Penjelasan** <br>
Perintah `chown` adalah perintah yang digunakan untuk mengubah kepemilikan (owner) dari sebuah file atau direktori. `areuka:areuka` Bagian ini menentukan pemilik (owner) dan grup (group) yang akan diberikan kepada file atau direktori yang diubah kepemilikannya. `/home/areuka/no4/log/metrics_${current_time}.log` dan `/home/areuka/no4/log/metrics_agg_${current_hour}.log` adalah alamat lengkap dari file yang akan memiliki kepemilikan yang diubah.

``` bash
chmod 600 /home/areuka/no4/log/metrics_${current_time}.log

chmod 600 /home/areuka/no4/log/metrics_agg_${current_hour}.log
```

**Penjelasan** <br>
Perintah `chmod` digunakan untuk mengubah hak akses (permissions) sebuah berkas atau direktori. `600` adalah mode atau hak akses yang ditentukan dalam bentuk angka oktal. Dalam mode ini, angka pertama "6" menunjukkan bahwa pemilik file (pengguna "areuka" dalam hal ini) diberikan izin untuk membaca dan menulis file (nilai "4" untuk membaca dan nilai "2" untuk menulis). Angka kedua "0" menunjukkan bahwa grup dan pengguna lainnya tidak memiliki izin untuk membaca, menulis, atau mengeksekusi file. Dengan demikian, hanya pemilik file yang memiliki izin penuh atas berkas ini. `/home/areuka/no4/log/metrics_${current_time}.log` dan `/home/areuka/no4/log/metrics_agg_${current_hour}.log` adalah alamat lengkap dari file yang akan memiliki hak akses yang diubah.


## Revisi

### Soal 2

#### Poin A

Terdapat perubahan saat user memasukkan email yang mana mengharuskan email agar bersifat unique sehingga ditambahkan baris kode seperti pada screenshot sebagai berikut:

![rev-1](/uploads/222578b2073ea045fb0d9c360e293ea5/rev-1.png)

**Penjelasan** <br>

1. Apabila user memasukkan email tanpa `@gmail.com`, maka akan muncul pesan `REGISTER FAILED - Invalid email format!`
2. Apabila user memasukkan email yang telah terdaftar sebelumnya, maka akan muncul pesan `REGISTER FAILED - Email already exist!`

**Output** <br>

![rev-out](/uploads/ee420590d75747d7db202eb79c04518d/rev-out.png)

Terdapat pemindahan baris kode conditional statement password ke dalam conditional statement email seperti pada screenshot sebagai berikut:

![pindah](/uploads/ae7b62d8906977f1112802c1975bf20b/pindah.png)

Terdapat penambahan pada pesan `REGISTER FAILED - ` saat password tidak memenuhi kriteria dan penghapusan ` ` (spacebar) agar tampilan log dalam file `auth.log` lebih rapi seperti pada screenshot sebagai berikut:

**Sebelum:**<br>
![rev-a-before](/uploads/a614f3036686a7d79e3b1badc273aeab/rev-a-before.png)

**Sesudah:**<br>
![rev-a-after](/uploads/6db02edcae8bbe6b940ceafe4e69ae1e/rev-a-after.png)

#### Poin B

Terdapat penambahan baris kode dan perubahan baris kode terkait direktori file `users.txt` seperti pada screenshot sebagai berikut:

![rev-2](/uploads/932ea73245f8d3235a1c074fae839f90/rev-2.png)

**Penjelasan** <br>

1. Mengecek apakah terdapat folder `users` pada direktori tersebut. Apabila tidak terdapat folder `users`, script akan membuat folder `users` lalu lanjut ke langkah berikutnya dan apabila terdapat folder `users`, script akan langsung lanjut ke langkah berikutnya.<br>
2. Mengubah direktori file `users.txt` sesuai dengan kriteria soal yang mana sebelumnya berada pada `./user.txt` menjadi `./users/users/txt`.<br>

#### Poin F

Menghapus `\n` agar pesan log yang terdapat dalam file `auth.log` yang ditampilkan lebih rapi seperti pada screenshot sebagai berikut:

**Sebelum:**<br>
![rev-before](/uploads/5b057385d70e040ded2f30a0f056094c/rev-before.png)

**Sesudah:**<br>
![rev-after](/uploads/054ebeb0df5395a64624f327aad0658d/rev-after.png)

### soal 3
#### poin b

Terdapat kesalahan pada script saat ingin menampilkan jumlah pengguna untuk setiap senjata

script sebelum revisi: <br>
![script_sebelum_revisi](/uploads/c165d90cee7a0587c49c86cf4e6cb432/script_sebelum_revisi.jpg)
output sebelum revisi: <br>
![output_sebelum_revisi](/uploads/161e9e353d9d9ce2e37c8e7cf6617b61/output_sebelum_revisi.jpg)
script setelah revisi: <br>
![script_setelah_revisi](/uploads/ed4ad0d9c8cd2031934668f83c08c27b/script_setelah_revisi.jpg)
output setelah revisi: <br>
![output_setelah_revisi](/uploads/364ed3453b802d2bad48ead34f133213/output_setelah_revisi.jpg)